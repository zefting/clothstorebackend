﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Models
{
    public partial class OrderShipped
    {
        public int OrderId { get; set; }
        public bool? Shipped { get; set; }

        public virtual Orders Order { get; set; }
    }
}
