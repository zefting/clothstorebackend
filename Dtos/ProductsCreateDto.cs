﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Dtos
{
    public partial class ProductsCreateDto
    {
        public string ProductName { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public double Price { get; set; }
    }
}
