﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Dtos
{
    public partial class CostumersCreateDto
    {
        
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
    }
}
