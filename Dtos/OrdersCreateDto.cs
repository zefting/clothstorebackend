﻿using System;
using System.Collections.Generic;
using clothstoreBackend.Models;

namespace clothstoreBackend.Dtos
{
    public partial class OrdersCreateDto
    {
        public int CostumerId { get; set; }
        public DateTime SoldDate { get; set; }
        public int SoldBy { get; set; }
        public int CampaignId { get; set; }
        public double OrderTotal { get; set; }
        public DateTimeOffset Created { get; set; }
        public List<int> ProductIds {get; set;}
        
        public virtual CostumersReadDto ReadCostumer { get; set; }
        public virtual EmployeesReadDto SoldByNavigation { get; set; }
    }
}
