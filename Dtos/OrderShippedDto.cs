﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Dtos
{
    public partial class OrderShippedDto
    {
        public int OrderId { get; set; }
        public byte Shipped { get; set; }

        public virtual OrdersReadDto Order { get; set; }
    }
}
