using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using clothstoreBackend.Models;
using clothstoreBackend.Services;
using Pomelo.EntityFrameworkCore.MySql;
namespace clothstoreBackend.Data
{
    public interface IStoreRepo
    {
        // save to database
        bool SaveChanges();

        // Orders
        IEnumerable<Orders> GetAllOrders();
        Orders GetOrderById (int orderId);
        List<Products> GetProductsByOrder (int orderId);
        void PlaceOrderProductRelation(List<OrdersProducts> orderProductList);
        void PlaceOrder(Orders orderModel);
        void SetOrderShipped (OrderShipped orderShipped);
        void UpdateShippedStatus(OrderShipped orderShipped);
        OrderShipped GetOrderShipped(int orderId);
        
        
        //products
        IEnumerable<Products> GetAllProducts();
        Products GetProductById(int ProductId);

        // relation table between orders and products
        IEnumerable<OrdersProducts> GetAllOrdersProducts();
        IEnumerable<OrderShipped> GetAllOrdersStatus();

        // Employees
        IEnumerable<Employees> GetAllEmployees();
        Employees GetEmployeeById (int emplyId);

        // get costumer 
        IEnumerable<Costumers> GetAllCostumers();
        Costumers GetCostumerById (int? emplyId);
        void SaveCostumer(Costumers costumerModel);

        
    }
}