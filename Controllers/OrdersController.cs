using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using clothstoreBackend.Models;
using clothstoreBackend.Dtos;
using clothstoreBackend.Data;
using clothstoreBackend.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace clothstoreBackend.Controllers
{   
    [Route("api/orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IStoreRepo _repository;
        private readonly IMapper _mapper;
        private readonly OrderConfirmationService _mail;
        private IHtmlBuilder _htmlBuilder;
        

        public OrdersController(IStoreRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
          
        }
        [HttpGet]
        [Route("all")]
        public ActionResult<IEnumerable<OrdersReadDto>> GetAllOrders()
        {
            var orderItems = _repository.GetAllOrders();
            var orderProductItem = _repository.GetAllOrdersProducts();
            var orderStatusItems = _repository.GetAllOrdersStatus();
           
            var orderDtos = _mapper.Map<IEnumerable<OrdersReadDto>>(orderItems);

            foreach(var order in orderDtos)
            {
                var products = new List<Products>();
                    foreach(var orderProduct in orderProductItem)
                    {
                        if(orderProduct.OrderId == order.OrderId)
                        {  
                            products.Add(_repository.GetProductById(orderProduct.ProductId));
                        }
                    }
                 
                var orderShipped = new OrderShipped();
                    foreach(var orderStatus in orderStatusItems)
                    {
                        if(orderStatus.OrderId == order.OrderId)
                        {
                            orderShipped.Shipped = orderStatus.Shipped;
                        }
                    }
                
                order.Products = products;
                order.OrderStatus = orderShipped;
            }
            return Ok(orderDtos);
        }

        [HttpGet("{orderId}")]
        public ActionResult<OrdersReadDto> GetOrderById(int orderId)
        {
            var id = orderId;
            var orderItem = _repository.GetOrderById(orderId);
            var products = _repository.GetProductsByOrder(orderId);
            var orderStatus = _repository.GetOrderShipped(orderId);
            if(orderItem != null)
            {
                var costumerItem = _repository.GetCostumerById(orderItem.CostumerId);
                var employee = _repository.GetEmployeeById(orderItem.SoldBy);
                var mapOrder = new OrdersReadDto();
                mapOrder.OrderId = orderItem.OrderId;
                mapOrder.SoldBy = orderItem.SoldBy;
                mapOrder.OrderTotal = orderItem.OrderTotal;
                mapOrder.Created = orderItem.Created;
                mapOrder.SoldDate =orderItem.SoldDate;
                mapOrder.Products = products;
                mapOrder.OrderStatus = orderStatus;
                mapOrder.Costumer = costumerItem;
                
                return Ok(mapOrder);
            }
            return NotFound();
        }
        [HttpPost]
        public ActionResult<OrdersReadDto> PlaceOrder(OrdersCreateDto ordersCreateDto)
        {
            // Store costumer info
            
            // save the incomming order
            var orderModel = new Orders();
            var costumerModel = new Costumers();
            costumerModel.Address = ordersCreateDto.ReadCostumer.Address;
            costumerModel.City = ordersCreateDto.ReadCostumer.City;
            costumerModel.Firstname = ordersCreateDto.ReadCostumer.Firstname;
            costumerModel.Lastname = ordersCreateDto.ReadCostumer.Lastname;
            costumerModel.ZipCode = ordersCreateDto.ReadCostumer.ZipCode;
            costumerModel.Email = ordersCreateDto.ReadCostumer.Email;
            _repository.SaveCostumer(costumerModel);
            _repository.SaveChanges();
            orderModel.SoldBy = ordersCreateDto.SoldBy;
            orderModel.CampaignId = ordersCreateDto.CampaignId;
            orderModel.CostumerId = costumerModel.CostumerId;
            orderModel.OrderTotal = ordersCreateDto.OrderTotal;
            orderModel.SoldDate = ordersCreateDto.SoldDate;
            
             
            _repository.PlaceOrder(orderModel);
            _repository.SaveChanges();
           

            // set orderShipped status
            var orderShipped = new OrderShipped();
            orderShipped.OrderId = orderModel.OrderId;
            orderShipped.Shipped = false;
            _repository.SetOrderShipped(orderShipped);
            _repository.SaveChanges();

            // save the orderproduct relation
            var orderProductList = new List<OrdersProducts>();
            foreach(var productId in ordersCreateDto.ProductIds)
            {
                var ordersProduct = new OrdersProducts();

                ordersProduct.OrderId = orderModel.OrderId;
                ordersProduct.ProductId = productId;

                orderProductList.Add(ordersProduct);
            }
            _repository.PlaceOrderProductRelation(orderProductList);
            _repository.SaveChanges();

            // get the new costumer id
            var costumer = _repository.GetCostumerById(orderModel.CostumerId);
            var returnItem = new OrdersReadDto();
            returnItem.OrderId = orderModel.OrderId;
            returnItem.SoldDate = orderModel.SoldDate;
            returnItem.SoldBy = orderModel.SoldBy;
            returnItem.Created = orderModel.Created;
            returnItem.OrderTotal = orderModel.OrderTotal;
            returnItem.Costumer = costumer;
            returnItem.SoldByNavigation = orderModel.SoldByNavigation;
            returnItem.soldproducts = orderProductList;

            // Generate Email html String
            var htmlString = _htmlBuilder.StringBuilder(orderProductList, ordersCreateDto.OrderTotal);

            // send email
            _mail.SendMail(costumer.Address,"orderconfirmation #" + orderModel.OrderId, htmlString);
            return returnItem;
            
        }

    }
}