﻿using System;
using System.Collections.Generic;
using clothstoreBackend.Models;
namespace clothstoreBackend.Dtos
{
    public partial class OrdersReadDto
    {
        public int OrderId { get; set; }
        public int CostumerId { get; set; }
        public DateTime SoldDate { get; set; }
        public int SoldBy { get; set; }
        public int? CampaignId { get; set; }
        public double OrderTotal { get; set; }
        public DateTime Created { get; set; }
        public string EmployeeName {get; set;}
        public virtual Costumers Costumer { get; set; }
        public virtual Employees SoldByNavigation { get; set; }
        public List<Products> Products {get; set;}
        public List<OrdersProducts> soldproducts {get; set;}
        public OrderShipped OrderStatus {get; set;}
    }
}
