﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Dtos
{
    public partial class ProductsReadDto
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public double Price { get; set; }
    }
}
