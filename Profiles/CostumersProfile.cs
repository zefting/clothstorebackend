using AutoMapper;
using clothstoreBackend.Dtos;
using clothstoreBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clothstoreBackend.Profiles
{
    public class CostumersProfile : Profile
    {
        public CostumersProfile()
        {
            CreateMap<Costumers, CostumersCreateDto>();
            CreateMap<CostumersCreateDto, Costumers>();

            CreateMap<CostumersReadDto, Costumers>();
            CreateMap<Costumers, CostumersReadDto>();

        }
    }
}