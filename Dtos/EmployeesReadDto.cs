﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Dtos
{
    public partial class EmployeesReadDto
    {
        public int EmployeeId { get; set; }
        public string Fistname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }

    }
}
