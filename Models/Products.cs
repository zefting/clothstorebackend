﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Models
{
    public partial class Products
    {
        public Products()
        {
            OrdersProducts = new HashSet<OrdersProducts>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }

        public virtual ICollection<OrdersProducts> OrdersProducts { get; set; }
    }
}
