using AutoMapper;
using clothstoreBackend.Dtos;
using clothstoreBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clothstoreBackend.Profiles
{
    public class OrdersProfile : Profile
    {
        public OrdersProfile()
        {
            CreateMap<Orders, OrdersCreateDto>();
            CreateMap<OrdersReadDto, Orders>();
        }
    }
}