using AutoMapper;
using clothstoreBackend.Dtos;
using clothstoreBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clothstoreBackend.Profiles
{
    public class OrderShippedProfile : Profile
    {
        public OrderShippedProfile()
        {
            CreateMap<OrderShipped, OrderShippedDto>();
            CreateMap<OrderShippedDto, OrderShipped>();
        }
    }
}