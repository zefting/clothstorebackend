﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Dtos
{
    public partial class OrderStatusDto
    {
        public byte Shipped { get; set; }

    }
}
