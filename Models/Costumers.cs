﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Models
{
    public partial class Costumers
    {
        public Costumers()
        {
            Orders = new HashSet<Orders>();
        }

        public int CostumerId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
