using System;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using clothstoreBackend.Data;
using clothstoreBackend.Models;

using Microsoft.Extensions.Configuration;

using AutoMapper;

// email client 
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace clothstoreBackend.Services
{
    public class OrderConfirmationService : IMailHelper
    {
        IConfiguration configuration;
        

        public OrderConfirmationService( IConfiguration configuration)
        {   
            this.configuration = configuration;
        }

         public void SendMail(string to, string subject, string messageContent)
        {
            string fromAddress = configuration["SmtpConfig:FromAddress"];
            string serverAddress = configuration["SmtpConfig:ServerAddress"];
            string username = configuration["SmtpConfig:Username"];
            string password = configuration["SmtpConfig:Password"];
            int port = Convert.ToInt32(configuration["SmtpConfig:Port"]);
            bool isUseSsl = Convert.ToBoolean(configuration["SmtpConfig:IsUseSsl"]);

            try
            {
                string from = "example@example.dk"; 
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(from, from));
                message.To.Add(new MailboxAddress(to, to));
                message.Subject = subject;
                message.Body = new TextPart("html")
                {
                    Text = messageContent
                };

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.Connect(serverAddress, port, isUseSsl);
                    client.Authenticate(username, password);
                    client.Send(message);
                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        
    }

}