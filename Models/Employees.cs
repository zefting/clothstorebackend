﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Models
{
    public partial class Employees
    {
        public Employees()
        {
            Orders = new HashSet<Orders>();
        }

        public int EmployeeId { get; set; }
        public string Fistname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
