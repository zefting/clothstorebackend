﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Models
{
    public partial class Orders
    {
        public Orders()
        {
            OrdersProducts = new HashSet<OrdersProducts>();
        }

        public int OrderId { get; set; }
        public int CostumerId { get; set; }
        public DateTime SoldDate { get; set; }
        public int SoldBy { get; set; }
        public int? CampaignId { get; set; }
        public double OrderTotal { get; set; }
        public DateTime Created { get; set; }

        public virtual Costumers Costumer { get; set; }
        public virtual Employees SoldByNavigation { get; set; }
        public virtual OrderShipped OrderShipped { get; set; }
        public virtual ICollection<OrdersProducts> OrdersProducts { get; set; }
    }
}
