using clothstoreBackend.Models;
using System.Collections.Generic;
namespace clothstoreBackend.Data
{
    public interface IHtmlBuilder
    {
         string StringBuilder(List<OrdersProducts> orderList, double OrderTotal);
    }
}