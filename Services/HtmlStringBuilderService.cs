using System;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using clothstoreBackend.Data;
using clothstoreBackend.Models;

using Microsoft.Extensions.Configuration;

using AutoMapper;

// email client 
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace clothstoreBackend.Services
{
    public class HtmlStringBuilderService : IHtmlBuilder
    {
        public string StringBuilder(List<OrdersProducts> orderList, double OrderTotal)
        {
            var OrderList = orderList; 
            var orderBuilder = new StringBuilder();
            orderBuilder.Append(@"<html>");
            orderBuilder.Append(@"<head>");
            //Load in bootstrap from cdn 
            orderBuilder.Append(@"<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>");
            // make the html responsive
            orderBuilder.Append(@"<meta name='viewport' content='width=device-width, initial-scale=1'>");
            orderBuilder.Append(@"</head>");
            orderBuilder.Append(@"<body>");
            orderBuilder.Append(@"<div class='container'>");
            orderBuilder.Append(@"<div class='row'>");
            orderBuilder.Append(@"<div class='col mx-auto'>");
            orderBuilder.Append(@"<table>");
            orderBuilder.Append(@"<thead>");
            orderBuilder.Append(@"<tr>");
            orderBuilder.Append(@"<th>Product</th>");
            orderBuilder.Append(@"<th>Price</th>");
            orderBuilder.Append(@" <th>Amount</th>");
            orderBuilder.Append(@" </tr>");
            orderBuilder.Append(@" </thead>");
            orderBuilder.Append(@"<tbody>");
            orderBuilder.Append(@"<tr>");
            foreach(var order in orderList)
            {
                orderBuilder.Append(@"<td>");
                orderBuilder.Append(@$"<{order.Product.ProductName}>");
                orderBuilder.Append(@"</td>");

                orderBuilder.Append(@"<td>");
                orderBuilder.Append(@$"<{order.Product.Price}>");
                orderBuilder.Append(@"</td>");

            }
            orderBuilder.Append(@"</tr>");
            orderBuilder.Append(@"<tr>");
            orderBuilder.Append(@"<td>");
            orderBuilder.Append(@"Total amount: ");
            orderBuilder.Append(@"</td>");
            orderBuilder.Append(@"<td>");
            orderBuilder.Append(@$"<{OrderTotal}>");
            orderBuilder.Append(@"</td>");
            orderBuilder.Append(@"</tr>");
            orderBuilder.Append(@"</tbody>");
            orderBuilder.Append(@"</table>");

            orderBuilder.Append(@"</div>");
            orderBuilder.Append(@"</div>");
            orderBuilder.Append(@"</div>");
            orderBuilder.Append(@"</body>");
            orderBuilder.Append(@"</html>");
            return orderBuilder.ToString();
        }
        
    }

}