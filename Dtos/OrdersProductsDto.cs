﻿using System;
using System.Collections.Generic;

namespace clothstoreBackend.Dtos
{
    public partial class OrdersProductsDto
    {
        public int OrderProductId { get; set; }
        public int OrderId { get; set; }
        public List<int> ProductIds { get; set; }

        public virtual OrdersReadDto ReadOrder { get; set; }
        public virtual OrdersCreateDto CreateOrder { get; set; }

        public virtual ProductsReadDto ReadProduct { get; set; }
         public virtual ProductsCreateDto CreateProduct { get; set; }
       
    }
}
