using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using clothstoreBackend.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using clothstoreBackend.Services;
using Pomelo.EntityFrameworkCore.MySql;

namespace clothstoreBackend.Data
{
    public class StoreRepoImpli : IStoreRepo
    {
        private readonly clothStoreContext _context;
        public StoreRepoImpli(clothStoreContext context)
        {
            _context = context;
        }
        // Orders
        public IEnumerable<Orders> GetAllOrders()
        {
            return _context.Orders.ToList();
        }
        public Orders GetOrderById(int orderId)
        {
            return _context.Orders.FirstOrDefault(order => order.OrderId == orderId);
        }

        public void PlaceOrder(Orders orderModel)
        {
            if(orderModel == null)
            {
                throw new ArgumentNullException(nameof(orderModel));
            }
            _context.Orders.Add(orderModel);
          /*  if(costumerModel ==null)
            {
                throw new ArgumentNullException(nameof(costumerModel));
            }
            _context.Costumers.Add(costumerModel);*/
        }

        public void PlaceOrderProductRelation(List<OrdersProducts> orderProductList)
        {
            if(orderProductList == null)
            {
                throw new ArgumentNullException(nameof(orderProductList));
            }
            _context.OrdersProducts.AddRange(orderProductList);
        }
        public void SetOrderShipped (OrderShipped orderShipped)
        {
            if(orderShipped == null)
            {
                throw new ArgumentNullException(nameof(orderShipped));
            }
            _context.OrderShipped.Add(orderShipped);
        }
        public OrderShipped GetOrderShipped(int orderId)
        {
            return _context.OrderShipped.FirstOrDefault(shipped => shipped.OrderId == orderId);
        }
        public void UpdateShippedStatus (OrderShipped orderShipped)
        {
            
        }      
        public IEnumerable<OrderShipped> GetAllOrdersStatus()
        {
            return _context.OrderShipped.ToList();
        }

        // Products 
        public IEnumerable<Products> GetAllProducts()
        {
            return _context.Products.ToList();
        }
        public Products GetProductById(int ProductId)
        {
            return _context.Products.FirstOrDefault(product => product.ProductId == ProductId);
        }

        // Order-Product relation

        public IEnumerable<OrdersProducts> GetAllOrdersProducts()
        {
            return _context.OrdersProducts.ToList();
        }

        public List<Products> GetProductsByOrder(int OrderId)
        {
            var orderProductItems = _context.OrdersProducts.ToList();
            var products = new List<Products>();
                foreach(var orderProduct in orderProductItems)
                {
                    if(orderProduct.OrderId == OrderId)
                    {
                        products.Add(orderProduct.Product);
                    }
                }
            return products;
        }

        public IEnumerable<Employees> GetAllEmployees()
        {
            return _context.Employees.ToList();
        }
        public Employees GetEmployeeById(int EmployeeId)
        {
            return _context.Employees.FirstOrDefault(employee => employee.EmployeeId == EmployeeId);
        }

        // Save to the database 
        public bool SaveChanges()
        {
            return(_context.SaveChanges() >=0);
        }

        public IEnumerable<Costumers> GetAllCostumers()
        {
            return _context.Costumers.ToList();
        }

        public Costumers GetCostumerById (int? CostumerId)
        {
            return _context.Costumers.FirstOrDefault(costumer => costumer.CostumerId == CostumerId);
        }

        public void SaveCostumer(Costumers costumerModel)
        {
            if(costumerModel == null)
            {
                throw new ArgumentNullException(nameof(costumerModel));
            }
             _context.Costumers.Add(costumerModel);
        }
    }
}